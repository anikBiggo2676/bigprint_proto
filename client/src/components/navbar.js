import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import {Link} from 'react-router-dom';
const NavBar = () => {
    return(
        <div>
        <AppBar position="static">
          <div>
            <Tab key="gallery"
              label="Gallery"
              component={Link}
              to='/gallery'
            />
            <Tab key="addimage"
              label="Add Image"
              component={Link}
              to='/addimage'
            />
          </div>
        </AppBar>
        </div>
    )
}
export default NavBar;
