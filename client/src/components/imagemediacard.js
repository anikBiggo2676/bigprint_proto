import React,{Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import axios from 'axios';
const styles = theme => ({
  paper: {
    position: 'relative',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
  card: {
    maxWidth: 345,
  },
  action:{
    width:'100%'
  },
  media: {
    // ⚠️ object-fit is not supported by IE11.
    objectFit: 'fill',
  },
});
function rand() {
  return 0;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}


class ImgMediaCard extends Component{

  state = {
    open: false,
    expanded:false
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  expand(){
    if(this.state.expanded){
      this.setState({
        expanded:false
      });
    }
    else{
      this.setState({
        expanded:true
      });
    }
  }
  render(){
    let cardContentStyle ;
    if(this.state.expanded){
      cardContentStyle= {
        visibilty: 'visible',
        transitionDuration: '0.3s'
      }
    }
    else{
      cardContentStyle= {
        visibilty: 'hidden',
        transitionDuration: '0.3s'
      }
    }
    const { classes,imageUrl,title,location,tags ,date ,oid,expanded} = this.props;
    return (
      <Card className={classes.card}>
        <CardActionArea className = {classes.action}>
          <CardMedia
            component="img"
            alt="Contemplative Reptile"
            className={classes.media}
            image={imageUrl}
            height="200"
            title="Contemplative Reptile"
          />
          <CardContent style={cardContentStyle} onClick={()=>this.expand()}>
            <Typography gutterBottom variant="h5" component="h2">
              {title}
            </Typography>
            <Typography component="p">
              {location}
            </Typography>
            <Typography component="p">
              {tags}
            </Typography>
            <Typography component="p">
              {date}
            </Typography>
          </CardContent>
          <Button onClick={this.handleOpen}>Open Modal</Button>
          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.open}
            onClose={this.handleClose}
          >
            <div style={getModalStyle()} className={classes.paper}>
              <CardMedia
                component="img"
                alt="Contemplative Reptile"
                className={classes.media}
                height="140"
                image={imageUrl}
                title="Contemplative Reptile"
              />
              <Typography variant="h6" id="modal-title">
                {title}
              </Typography>
              <Typography variant="subtitle1" id="simple-modal-description">
                {location}
              </Typography>
              <Typography variant="subtitle1" id="simple-modal-description">
                {tags}
              </Typography>
            </div>
        </Modal>
        </CardActionArea>
      </Card>
    );
  }

}

ImgMediaCard.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(ImgMediaCard);
