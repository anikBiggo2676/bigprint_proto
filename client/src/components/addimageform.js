import React, { Component } from "react";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import axios from "axios";
import MultipleSelect from "./multipleselect.js";

class AddImageForm extends Component {
  state = {
    loading: true,
    replySuccess: "",
    replyMessage: "",
    tags: [],
    selected: []
  };
  componentDidMount() {
    axios
      .get("/posts/tags")
      .then(res => {
        console.log("res:", res.data);
        this.setState({
          tags: res.data.tags
        });
      })
      .catch(err => {
        console.log("error:", err);
      });
  }

  handleSelectedChange = async e => {
    await this.setState({
      selected: e.target.value
    });
    console.log("selected:", this.state.selected);
  };
  async onClickAddTag() {
    let newTagInput = document.getElementById("new-tags-input");
    if (newTagInput.value == "" || newTagInput.value.replace(/\s/g, "") == "") {
      await this.setState({
        replyMessage: "Type a tag before submitting"
      });
    } else {
      await this.setState({
        tags: [newTagInput.value.replace(/\s/g, ""), ...this.state.tags],
        replyMessage: "Tag added in the options"
      });
    }
    console.log(this.state.tags);
    console.log(this.refs.taginput2);
    newTagInput.value = "";
  }
  onClickPostData() {
    let formData = new FormData();
    let photoInput = document.querySelector("#photo-input");
    let titleInput = document.querySelector("#title-input");
    let locationInput = document.querySelector("#location-input");
    let tagsInput = this.state.selected;
    console.log("tagsinput:", tagsInput);
    formData.append("photo", photoInput.files[0]);
    formData.append("title", titleInput.value);
    formData.append("location", locationInput.value);
    formData.append("tags", tagsInput);

    axios
      .post("/posts/add", formData, {
        headers: { "Content-Type": "multipart/form-data" }
      })
      .then(res => {
        console.log("data submitted.");
        console.log(res);
        this.setState({
          replySuccess: true,
          replyMessage: "File upload successful"
        });
      })
      .catch(err => {
        console.log("error:", err);
        this.setState({
          replySuccess: false,
          replyMessage: "Failed to upload file"
        });
      });
  }
  render() {
    return (
      <div>
        <p>Add Image</p>
        <p>{this.state.replyMessage}</p>
        <FormControl>
          <Input type="file" name="photo" id="photo-input" />
          <Input
            placeholder="Title"
            type="text"
            name="title"
            id="title-input"
          />
          <Input
            placeholder="location"
            type="text"
            name="location"
            id="location-input"
          />
          <MultipleSelect
            tags={this.state.tags}
            handleSelectedChange={this.handleSelectedChange}
          />
          <Button
            color="primary"
            variant="contained"
            onClick={() => this.onClickPostData()}
          >
            Submit
          </Button>
          <Input
            placeholder="Add a new tag for your product"
            type="text"
            name="new-tag"
            id="new-tags-input"
          />
          <Button
            color="primary"
            variant="contained"
            onClick={() => this.onClickAddTag()}
          >
            Add new Tag
          </Button>
        </FormControl>
      </div>
    );
  }
}

export default AddImageForm;
