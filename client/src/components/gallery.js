import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import axios from 'axios';
import MultipleSelect from './multipleselect.js';
import ImageMediaCard from './imagemediacard.js';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root:{
    flex:5,
    display: 'flex',
    flexDirection: 'row',
    flexWrap:'wrap',
    width: '80%',
    margin:'3em auto',
    justifyContent:'flex-start'
  },
  card: {
    margin:'3em',
    minWidth:'25%',
    display:'flex',
    flexDirection:'column',
    justifyContent:'center'
  },
  delButton:{
    width:'25%',
    margin:'auto'
  }
});

class Gallery extends Component{
  state={
    loading:true,
    posts:[]
  };
  componentDidMount(){
    this.fetchAndShowData();
  }
  fetchAndShowData(){
    axios.get('/posts')
      .then(res=>{
        console.log('response:',res.data);
        this.setState({
          posts: res.data
        });
      })
      .catch((err)=>{
        console.log('Error occurred while fetching data',err)
      });
  }
  onClickDeletePost(oid){
    axios.delete(`/posts/${oid}`)
      .then(res=>{
        console.log(res);
        const upDatedPosts = this.state.posts.filter(i => i._id !== oid);
        this.setState({
          posts: upDatedPosts
        });
      })
      .catch(err=>console.log(err))
  }
  render(){
    const {classes} = this.props;
    const renderablePosts = this.state.posts.map((item,idx)=>{
      return(
        <div className={classes.card}>
        <ImageMediaCard
          key={idx}
          imageUrl={`http://localhost:5000/uploads/${item.imageUrl}`}
          title = {item.title}
          location = {item.location}
          tags = {item.tags}
          oid = {item._id}
        />
        <Button className={classes.delButton}color="primary" variant="contained" onClick={()=>this.onClickDeletePost(item._id)}>Delete</Button>
        </div>
      );
    });
    return(
      <div className={classes.root}>
        {renderablePosts}
      </div>
    );
  }
}

export default withStyles(styles)(Gallery);
