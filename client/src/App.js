import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Navbar from './components/navbar.js';
import Landing from './components/landing.js';
import AddImageForm from './components/addimageform.js';
import Gallery from './components/gallery.js';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';

class App extends Component {
  render() {
    return (
      <Router>
        <div className = "App">
          <Navbar/>
          <Route exact path="/" component={Landing} />
          <Route path="/addimage" component={AddImageForm} />
          <Route path="/gallery" component={Gallery} />
        </div>
      </Router>
    );
  }
}

export default App;
