const express = require ('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
//importing routes
const posts = require('./routes/api/posts.js');

//connecting to mongodb server (host : mlab)
mongoose
  .connect('mongodb://root:Anik2676@ds125273.mlab.com:25273/bigprint_proto')
  .then(() => console.log('MongoDB Connected...'))
  .catch(err => console.log(err));
// Using bodyparser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Using imported routes
app.use('/posts',posts);
app.use('/uploads', express.static(__dirname + '/uploads'));
//server port config
const port = 5000;
app.listen(port , ()=>{
  console.log(`Server running on port : ${port}`);
});
