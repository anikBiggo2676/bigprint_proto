const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _ = require('lodash');
// Create Schema
const PostSchema = new Schema({
  imageUrl: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  location: {
    type: String,
    required: true
  },
  tags:{
    type: [String]
  },
  date: {
    type: Date,
    default: Date.now
  }
});

PostSchema.path('tags').validate(function (v) {
    return _.intersection(v, this.tags).length > 0
}, 'Post must have at least one tag !');

module.exports = Post = mongoose.model('post', PostSchema);
