const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');


//importing Post model
const Post = require('../../models/post.js');
//config multer storage
const storage = multer.diskStorage({
  destination : 'uploads/',
  filename: function (req, file, cb) {
    cb(null, file.fieldname + Date.now()+path.extname(file.originalname));
  }
})
//config upload options
const upload = multer({
  storage: storage ,
  limits : {fileSize:2000000},
  fileFilter : function(req,file,cb){
    validateFile(file,cb);
  }
}).single('photo');
//file validation
function validateFile(file,cb){
  const filetypes = /png|jpg|jpeg|gif/;
  const isExtNameValid = filetypes.test(path.extname(file.originalname.toLowerCase()));
  const isMimeTypeValid = filetypes.test(file.mimetype);
  // console.log(isExtNameValid);
  // console.log(file.mimetype);
  // console.log(isMimeTypeValid);
  if (isExtNameValid && isMimeTypeValid){
    cb(null, true);
  }
  else{
    cb('Please upload images only.');
  }
}


//routing
router.get('/test',(req,res)=>{
  res.send({
    message: "Router working"
  });
});

router.get('/',(req,res)=>{
  Post.find()
    .sort({ date: -1 })
    .then(posts => res.json(posts))
    .catch(err => res.status(404).json({ nopostsfound: 'No posts found' }));
});
router.get('/tags',(req,res)=>{
  let tagsArray=[];
  Post.find({},{tags:true,_id:false})
    .then((resultArray)=> {
      for(let i=0;i<resultArray.length;i++){
        let singlePostTags = resultArray[i].tags;
        for(let j=0;j<singlePostTags.length;j++){
          tagsArray.push(singlePostTags[j]);
        }
      }
      //console.log(tagsArray);
      res.json({tags:tagsArray});
    })
    .catch(err => res.status(404).json({ notagsfound: 'No tags found' ,error:err}));
});

router.post('/add',(req,res)=>{
  upload(req, res, function (err) {
    if (err) {
      return res.status(400).send({file : err});
    }
    else{
      let tagsArray= req.body.tags.replace(/\s/g,'').split(',').filter(item=> item != "");
      console.log(tagsArray.length , tagsArray);
      //console.log(req.file);
      // /console.log(req.file.filename);
      if(req.file==undefined){
        return res.status(400).send({file : "Please select a file."});
      }
      const newPost = new Post({
        imageUrl:req.file.filename,
        title: req.body.title,
        location: req.body.location,
        tags:tagsArray
      });
      newPost.save()
      .then(post => {
        return res.status(200).send({success:true,postedData:post,file:'Uploaded successfully'});
      })
      .catch(err=>{
        return res.status(400).send({success:false,error:err});
      });
    }
  });
});
router.delete('/:id',(req, res) => {
      Post.findById(req.params.id)
        .then(post => {
          post.remove()
          .then(() => {
            res.json({ success: true,message:'deleted the post' })
          })
          .catch(err=>console.log(err));
        })
        .catch(err => res.status(404).json({ postnotfound: 'No post found' }));
  }
);


module.exports = router;
